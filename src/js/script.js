$(document).ready(function() {
    // Window on load
    $(window).on('load', function() {
        $('.slide').owlCarousel({
            loop: true,
            nav: true,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 1
                },
                1000: {
                    items: 1
                }
            }
        });
        $('.mean__wrap').owlCarousel({
            loop: true,
            nav: true,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 1
                },
                1000: {
                    items: 1
                }
            }
        });
        $('.customer').owlCarousel({
            loop: true,
            nav: true,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 1
                },
                1000: {
                    items: 1
                }
            }
        });
        $('.news').owlCarousel({
            loop: true,
            nav: true,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 1
                },
                1000: {
                    items: 1
                }
            }
        })
        $('.tabcontent').owlCarousel({
            loop: true,
            nav: true,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 1
                },
                1000: {
                    items: 1
                }
            }
        })
        $('.section1__b--slide').owlCarousel({
                loop: true,
                margin: 10,
                nav: true,
                responsive: {
                    0: {
                        items: 3
                    },
                    600: {
                        items: 3
                    },
                    1000: {
                        items: 3
                    }
                }
            })
            //chuc section1__left--2
        $('#spdetail .section1__left--2 ul').find('li:first').show().addClass('activee');
        $('#spdetail .section1__left--2 p:first').show();
        $('#spdetail .section1__left--2 ul').find('li:first').click(function() {
            $('#spdetail .section1__left--2 li:first').next().removeClass('activee');
            $('#spdetail .section1__left--2 li:first').addClass('activee');
            $('#spdetail .section1__left--2 p:first').next().hide();
            $('#spdetail .section1__left--2 p:first').show();
        });
        $('#spdetail .section1__left--2 ul').find('li:first').next().click(function() {
            $('#spdetail .section1__left--2 li:first').removeClass('activee');
            $('#spdetail .section1__left--2 li:first').next().addClass('activee');
            $('#spdetail .section1__left--2 p:first').hide();
            $('#spdetail .section1__left--2 p:first').next().show();
        });
    });
    var stt = 0;
    var next, prev;
    var x = parseInt($("#section4 img:first-child").attr("stt"));
    var y = parseInt($("#section4 img:last-child").attr("stt"));
    // alert(x);
    $('#section4 img').each(function() {
        if ($(this).is(':visible')) {
            stt = parseInt($(this).attr("stt"));
        }
    });
    $('#nextt').click(function() {
        next = ++stt;
        if (next == y) {
            stt = -1;
        }
        $("#section4 img").hide();
        $("#section4 img").eq(next).show();
        $("#section4 li").removeClass('active');
        $("#section4 li").eq(next).addClass('active');
    });
    $('#prevv').click(function() {

        if (stt == 0) {
            stt = y;
        }
        prev = --stt;
        $("#section4 img").hide();
        $("#section4 img").eq(prev).show();
        $("#section4 li").removeClass('active');
        $("#section4 li").eq(prev).addClass('active');
    });
    // $('.owl-carousel').owlCarousel({
    //     items: 1,
    //     loop: false,
    //     center: true,
    //     margin: 0,
    //     URLhashListener: true,
    //     autoplayHoverPause: true,
    //     startPosition: 'URLHash'
    // });
    // setInterval(function() {
    //     $('#nextt').click();
    // }, 1000);
    //start by Tuan
    $(window).resize(function(event) {
        var windowWidth = $(this).width();
        if (windowWidth > 720) {
            $('.header_content ul').css({
                display: 'block'
            });
        } else {
            $('.header_content ul').css({
                display: 'none'
            });
        }
    });

    //start nav-bar by Tuan
    $('.bar-customer').click(function(e) {
        e.preventDefault();
        $(this).toggleClass('bar-active');
        $('.header_content ul').slideToggle(200);
    });
    $(window).scroll(function(event) {
        var windowWidth = $(this).width();
        if (windowWidth <= 720) {
            $('.bar-customer').removeClass('bar-active');
            $('.header_content ul').slideUp(500);
        }
    });
    //start search by Tuan
    $('.wrap_navmore .fa-sistrix').click(function(e) {
        e.preventDefault();
        $('.search_layer').toggleClass('search_active');
    });
    $('.search_layer .fa-times').click(function(e) {
        e.preventDefault();
        $('.search_layer').removeClass('search_active');
    });
    //end search by Tuan

    //start back to top by Tuan
    $('.backtop').click(function(e) {
        e.preventDefault();
        $('html,body').animate({ scrollTop: 0 }, 1000);
    });
    // end back to top by Tuan

    var currentScroll;
    $(window).scroll(function(event) {
        /* Act on the event */
        currentScroll = $(window).scrollTop();
        if (currentScroll == 0) {
            $('.backtop').css({
                display: 'none',
            });
        } else {
            $('.backtop').css({
                display: 'block',
            });
        }
    });



    // menu - san pham - new - Khanh
    $(".click-dropdown_menu").click(function() {
        $(this).find(".dropdown_menu-sanpham").slideToggle();
        $(this).find("span").toggleClass("active-dropdown");
    });
    // fancy box - dang nhap dang ki -new - Khanh
    $(".li-dangki").click(function(event) {
        $("section#dangnhap").css("display", "none");
        $("section#dangki").css("display", "block");
        return false;
    });
    $(".li-dangnhap").click(function(event) {
        $("section#dangnhap").css("display", "block");
        $("section#dangki").css("display", "none");
        return false;
    });
    // Khanh fancybox gio hang
});
// });
//section4__home
// $(document).ready(function() {

// });
//section3

var slideIndex = 1;
showSlides(slideIndex);

// function plusSlides(n) {
//     showSlides(slideIndex += n);
// }

function currentSlide(n) {
    showSlides(slideIndex = n);
}

function showSlides(n) {
    var i;
    var slides = document.getElementsByClassName("section3__box2--img");
    if (n > slides.length) { slideIndex = 1 }
    if (n < 1) { slideIndex = slides.length }
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    slides[slideIndex - 1].style.display = "block";
}
var y = 1;
show(y);
var index = 1;
show(index);

function current(n) {
    show(index = n);
}

function show(n) {
    var i;
    var x = document.getElementsByClassName("show");
    if (n > x.length) { index = 1 }
    if (n < 1) { index = x.length }
    for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";
    }
    x[index - 1].style.display = "block";
}


function openPage(pageName) {
    var i, tabcontent;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    document.getElementById(pageName).style.display = "block";
}
document.getElementById("defaultOpen").click();